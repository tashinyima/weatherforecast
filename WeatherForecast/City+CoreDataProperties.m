//
//  WACity+CoreDataProperties.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "City+CoreDataProperties.h"

@implementation City (CoreDataProperties)

@dynamic wacityID;
@dynamic name;
@dynamic latitude;
@dynamic longitude;
@dynamic countrycode;
@dynamic isfavourited;

@end
