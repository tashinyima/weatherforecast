//
//  WAConstants.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//


//Content urls
#define CITIES_LIST_ZIPPED @"http://bulk.openweathermap.org/sample/city.list.json.gz"
#define CITIES_LIST @"http://openweathermap.org/help/city_list.txt"
#define WEATHER_BY_NAME @"http://api.openweathermap.org/data/2.5/weather?q=%@&APPID=%@"
#define FORECAST_WEATHER_BY_ID @"http://api.openweathermap.org/data/2.5/forecast?id=%@&APPID=%@"
//Keys
#define OWA_APP_KEY @"0ddd8af45b1d5c6edcca23ed5cb6ac67"
