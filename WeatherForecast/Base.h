//
//  WABase.h
//  
//
//  Created by Tashi.Niyma on 3/17/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Base : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+(NSString *)entityIdentificationAttribute;
+(NSString *)responseIdentificationAttribute;
+(NSDictionary *)mapping;
+(NSDictionary *)relations;

@end

NS_ASSUME_NONNULL_END

#import "Base+CoreDataProperties.h"
