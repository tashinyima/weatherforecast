//
//  WACity.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Base.h"
NS_ASSUME_NONNULL_BEGIN

@interface City : Base



@end

NS_ASSUME_NONNULL_END

#import "City+CoreDataProperties.h"
