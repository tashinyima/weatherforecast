//
//  ForecastService.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import "ForecastService.h"
@interface ForecastService()
@property (nonatomic, strong) GetForecastSuccessBlock successBlock;
@property (nonatomic, strong) GetForecastFailureBlock failureBlock;
@property (nonatomic, strong) NSString * url;
@end
@implementation ForecastService

-(instancetype)initWithUrl:(NSString *)url success:(GetForecastSuccessBlock)successHandler failure:(GetForecastFailureBlock)failureHandler{
    self = [super init];
    if(self){
        _url = url;
        _successBlock = successHandler;
        _failureBlock = failureHandler;
    }
    return self;
}

-(void)startProcess{
    NSURLSessionTask * getTask = [self createDataTask:[self createRequest:_url]];
    [getTask resume];
}
-(NSURLRequest *)createRequest:(NSString *)urlStr{
    NSURL * url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30.0];
    return request;
}
-(NSURLSessionDataTask *)createDataTask:(NSURLRequest *)request{
    __weak ForecastService * weakSelf = self;
    NSURLSessionDataTask *getTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error){
            if(weakSelf.failureBlock)
                weakSelf.failureBlock(error);
        }else{
            NSError * jsonError;
            id forecastDict =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
            if(jsonError){
                if(weakSelf.failureBlock)
                    weakSelf.failureBlock(jsonError);
                
            }else{
                Forecast * forecast = [[Forecast alloc] initWithDictionary:forecastDict];
                if(weakSelf.successBlock)
                    weakSelf.successBlock(forecast);
            }
        }
    }];
    
    return getTask;
}
@end
