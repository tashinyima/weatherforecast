//
//  DaysDataSource.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import "DaysDataSource.h"
#import "DayCollectionViewCell.h"

@interface DaysDataSource()
@property (nonatomic, strong) NSArray * days;
@property (weak, nonatomic) id<DayDelegate> delegate;
@end

@implementation DaysDataSource
-(instancetype) initWithDays:(NSArray *)days delegate:(id<DayDelegate>)delegate{
    self = [super init];
    if(self){
        _days = days;
        _delegate = delegate;
    }
    return self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _days.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DayCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Day" forIndexPath:indexPath];
    [cell setDate:_days[indexPath.item]];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(_delegate && [_delegate respondsToSelector:@selector(dayTapped:)]){
        [_delegate dayTapped:_days[indexPath.item]];
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(120, 30);
}
@end
