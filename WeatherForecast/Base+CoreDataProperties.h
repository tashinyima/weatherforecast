//
//  WABase+CoreDataProperties.h
//  
//
//  Created by Tashi.Niyma on 3/17/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Base.h"

NS_ASSUME_NONNULL_BEGIN

@interface Base (CoreDataProperties)


@end

NS_ASSUME_NONNULL_END
