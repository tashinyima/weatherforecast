//
//  WeatherDataSource.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//


#import "ForecastDataSource.h"
#import "ForecastCollectionViewCell.h"

@interface ForecastDataSource()
@property (nonatomic, strong) NSArray * forecastItems;
@end

@implementation ForecastDataSource
-(instancetype)initWithForecastItems:(NSArray *)forecastItems{
    self = [super init];
    if(self){
        _forecastItems = forecastItems;
    }
    return self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _forecastItems.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ForecastCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Forecast" forIndexPath:indexPath];
    [cell setForecast:_forecastItems[indexPath.item]];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width - 40, 200);
}
@end
