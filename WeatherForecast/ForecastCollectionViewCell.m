//
//  WeatherCollectionViewCell.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import "ForecastCollectionViewCell.h"
#import "Forecast.h"
#import "NSDate+Utils.h"

@interface ForecastCollectionViewCell()

@end
@implementation ForecastCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _vwBack.layer.cornerRadius = 5;
    _vwBack.layer.borderWidth = 1;
    _vwBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
}
-(void)setForecast:(ForecastItem * )forecast{
    _forecast = forecast;
    //Clear Snow Clouds Snow
    //cloud rain snow storm sunny
    if([_forecast.weather.firstObject.main isEqualToString:@"Clear"]){
        [_imgIcon setImage:[UIImage imageNamed:@"sunny"]];
    }else if([_forecast.weather.firstObject.main isEqualToString:@"Snow"]){
        [_imgIcon setImage:[UIImage imageNamed:@"snow"]];
    }else if([_forecast.weather.firstObject.main isEqualToString:@"Clouds"]){
        [_imgIcon setImage:[UIImage imageNamed:@"cloud"]];
    }else if([_forecast.weather.firstObject.main isEqualToString:@"Rain"]){
        [_imgIcon setImage:[UIImage imageNamed:@"rain"]];
    }else if([_forecast.weather.firstObject.main isEqualToString:@"Extreme"]){
        [_imgIcon setImage:[UIImage imageNamed:@"storm"]];
    }
    _lblTime.text = _forecast.dt.timeString;
    _lblWeatherMain.text = _forecast.weather.firstObject.main;
    _lblWeatherDesc.text = _forecast.weather.firstObject.desc;
    _lblWeatherTemp.text = [NSString stringWithFormat:@"%.2f K",_forecast.main.temp];
    _lblWeatherTempMin.text = [NSString stringWithFormat:@"%.2f K",_forecast.main.temp_min];
    _lblWeatherTempMax.text = [NSString stringWithFormat:@"%.2f K",_forecast.main.temp_max];
    _lblWeatherSpeed.text = [NSString stringWithFormat:@"%.2f",_forecast.wind.speed];
    _lblWeatherSpeedDeg.text = [NSString stringWithFormat:@"%.2f",_forecast.wind.deg];
    _lblWeatherHumidity.text = [NSString stringWithFormat:@"Humidity %d",_forecast.main.humidity];
    _lblWeatherPressure.text = [NSString stringWithFormat:@"Pressure %.2f",_forecast.main.pressure];
}
@end
