//
//  ForecastService.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Forecast.h"

typedef void (^GetForecastSuccessBlock)(Forecast * forecast);
typedef void (^GetForecastFailureBlock)(NSError * error);

@interface ForecastService : NSObject

@property (nonatomic, readonly) GetForecastSuccessBlock successBlock;
@property (nonatomic, readonly) GetForecastFailureBlock failureBlock;

-(instancetype)initWithUrl:(NSString *)url success:(GetForecastSuccessBlock)successHandler failure:(GetForecastFailureBlock)failureHandler;

-(NSURLSessionDataTask *)createDataTask:(NSURLRequest *)request;
-(NSURLRequest *)createRequest:(NSString *) url;
-(void)startProcess;


@end
