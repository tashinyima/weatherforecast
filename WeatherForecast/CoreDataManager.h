//
//  WACoreDataManager.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Base.h"
/**
 *  @brief Manager for core data related tasks
 */
@interface CoreDataManager : NSObject{
    NSManagedObjectContext *_managedObjectContext;
    NSManagedObjectContext *_mainManagedObjectContext;
    NSManagedObjectModel *_managedObjectModel;
    NSPersistentStoreCoordinator *_persistentStoreCoordinator;
    
}
/**
 *  @brief Private queue MOC
 */
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
/**
 *  @brief Main queue MOC
 */
@property (readonly, strong, nonatomic) NSManagedObjectContext *mainManagedObjectContext;
/**
 *  @brief Managed object model (typicode.momd)
 */
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
/**
 *  @brief Sqlite type store coordinator
 */
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

/**
 *  @brief Saves entity by creating/updating a record
 *
 *  @param name       Name of entity
 *  @param dictionary Dictionary representing the entity that will be used to populate managed object
 *
 *  @return A created/saved managed object
 */
- (Base *)saveEntity:(NSString *)name withPayload:(NSDictionary *)dictionary;
/**
 *  @brief Saves all dictionaries passed, deletes all entities of given type before performing save
 *
 *  @param name         Entity name
 *  @param dictionaries Array of dictionaries representing the entity that will be used to populate managed object
 *
 *  @return An array of created managed objects
 */
-(NSArray *)saveAllEntities:(NSString *)name withPayloads:(NSArray *)dictionaries;
/**
 *  @brief Entities of given name with given pageno offset and pagesize
 *
 *  @param name     Name of entity
 *  @param pageno   Page number
 *  @param pagesize Page size
 *
 *  @return Entities matching name at pageno with given page size
 */
-(NSArray *)entities:(NSString *)name page:(int)pageno size:(int)pagesize;
/**
 *  @brief All entities of given name
 *
 *  @param name Name of entity
 *
 *  @return Returns all entities of given name
 */
-(NSArray *)entities:(NSString *)name;

/**
 *  @brief All entities of given name fulfilling predicate
 *
 *  @param name Name of entity
 *
 *  @param predicate Predicate with criteria
 *
 *  @return Returns all entities of given name
 */
-(NSArray *)entities:(NSString *)name withPredicate:(NSPredicate * )predicate;
/**
 *  @brief Saves context
 */
- (void)saveContext;
/**
 *  @brief Application Documents directory
 *
 *  @return Application Documents directory
 */
- (NSURL *)applicationDocumentsDirectory;

@end
