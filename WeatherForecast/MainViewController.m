//
//  ViewController.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import "MainViewController.h"
#import "ForecastViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "City.h"
#import "CitiesService.h"
#import "Constants.h"
#import "CoreDataManager.h"

@interface MainViewController () <UISearchBarDelegate>
@property (nonatomic, strong) NSMutableArray * favoritedCities;
@property (nonatomic, strong) NSMutableArray * nonFavoritedCities;
@property (nonatomic, assign) BOOL isAddingStation;
@property (weak, nonatomic) IBOutlet UIView *vwSearchContainer;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (nonatomic, strong) CitiesService * cityService;
@property (nonatomic, strong) CoreDataManager * coredataManager;
- (IBAction)doneClicked:(id)sender;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _coredataManager = [[CoreDataManager alloc] init];
    self.title = @"Locations";
    self.navigationItem.rightBarButtonItem = [self createAddButton];
    [self loadCities];
    [self showAddedStations];
}

- (UIBarButtonItem *)createAddButton{
    //Add button to add new stations
    UIBarButtonItem * btnAdd = [[UIBarButtonItem alloc] initWithTitle:@"++Station" style:UIBarButtonItemStyleDone target:self action:@selector(addStation:)];
    return btnAdd;
}

- (void)loadCities{
    //Download and process cities
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    //If cities parsed then don't do it again
    if(![ud valueForKey:@"Cities_Parsed"] && ![[ud valueForKey:@"Cities_Parsed"] boolValue]){
    __weak MBProgressHUD * progressHUD = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    _cityService = [[CitiesService alloc] initWithUrl:CITIES_LIST success:^(NSArray *cities) {
        [ud setValue:@(YES) forKey:@"Cities_Parsed"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [progressHUD hideAnimated:YES];
        });

    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [progressHUD hideAnimated:YES];
        });
    } rawData:nil];
    //[_cityService startProcess];
    //Parse local list of cities instead of service call. Takes long
    NSError * error;
    [_cityService parseTSVCities:[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"city_list" ofType:@"txt"] encoding:NSWindowsCP1251StringEncoding error:&error]];
    
    }
}


- (void)addStation:(id)sender{
    //Display search bar and enable searching
    _isAddingStation = YES;
    _vwSearchContainer.hidden = NO;
    [self.tableView reloadData];
}

- (void)showAddedStations{
    //Hide search bar and display favourited stations
    self.nonFavoritedCities = nil;
    self.favoritedCities = [self getFavoritedCities];
    _isAddingStation = NO;
    _vwSearchContainer.hidden = YES;
    [self.tableView reloadData];

}
- (NSMutableArray *)getFavoritedCities{
    //Get favorited cities
    return [NSMutableArray arrayWithArray:[_coredataManager entities:@"City" withPredicate:[NSPredicate predicateWithFormat:@"isfavourited == YES"]]];
}

- (NSMutableArray *)getNonFavoritedCitiesByName:(NSString *)name{
    //Get non-favorited cities based on search text
    return [NSMutableArray arrayWithArray:[_coredataManager entities:@"City" withPredicate:[NSPredicate predicateWithFormat:@"(name BEGINSWITH[cd] %@) AND (isfavourited == NO OR isfavourited == NULL)",name]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_isAddingStation){
        return self.nonFavoritedCities.count;
    }else{
        return self.favoritedCities.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    City * city = nil;
    if(_isAddingStation){
        city = self.nonFavoritedCities[indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%@, %@",city.name, city.countrycode];
        cell.detailTextLabel.text = @"Add to Favorites";
        cell.accessoryType = UITableViewCellAccessoryNone;
    }else{
        city =  self.favoritedCities[indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%@, %@",city.name, city.countrycode];
        cell.detailTextLabel.text = @"";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!_isAddingStation){
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!_isAddingStation){
        if(editingStyle == UITableViewCellEditingStyleDelete){
            //Remove city from favorite and save
            City * city = self.favoritedCities[indexPath.row];
            city.isfavourited = @(NO);
            [_coredataManager saveContext];
            //Remove favorited city and remove corresponding cell
            [self.favoritedCities removeObject:city];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            
        }
    }
}

#pragma mark - UITableViewDataSource
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_isAddingStation){
        //Momentarily check the cell
        UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        //Add city to favorite and save
        City * city = self.nonFavoritedCities[indexPath.row];
        city.isfavourited = @(YES);
        [_coredataManager saveContext];
        //Remove city and remove corresponding cell
        [self.nonFavoritedCities removeObject:city];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }else{
        [self performSegueWithIdentifier:@"Detail" sender:self];
        
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length == 0){
        [self showAddedStations];
    }
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    //Search non-favorited cities and reload
    [searchBar resignFirstResponder];
    self.nonFavoritedCities = [self getNonFavoritedCitiesByName:searchBar.text];
    [self.tableView reloadData];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];    
    [self showAddedStations];
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"Detail"]){
        //Open weather details for selected station
        City * city = self.favoritedCities[self.tableView.indexPathForSelectedRow.row];
        ForecastViewController * forecastVC = (ForecastViewController *)segue.destinationViewController;
        forecastVC.city = city;
    }
}

- (IBAction)doneClicked:(id)sender {
    [_searchBar resignFirstResponder];
    [self showAddedStations];
    
}
@end
