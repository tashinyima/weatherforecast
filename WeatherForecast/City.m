//
//  WACity.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import "City.h"

@implementation City

+(NSString *)responseIdentificationAttribute{
    return @"id";
}
+(NSString *)entityIdentificationAttribute{
    return @"wacityID";
}
+(NSDictionary *)mapping{
    return @{@"wacityID":@"id",@"name":@"nm",@"latitude":@"lat", @"longitude":@"lon",@"countrycode":@"countryCode"};
}
+(NSDictionary *)relations{
    return @{@"userId":@"poster"};
}@end
