//
//  DayCollectionViewCell.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import "DayCollectionViewCell.h"
#import "NSDate+Utils.h"
@interface DayCollectionViewCell()
@end
@implementation DayCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 5;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 1;
    // Initialization code
}
-(void)setDate:(NSDate *)date{
    _date = date;
    _lblDay.text = _date.dateString;
}
@end
