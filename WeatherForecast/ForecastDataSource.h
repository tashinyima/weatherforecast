//
//  WeatherDataSource.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  <UIKit/UIKit.h>



@interface ForecastDataSource : NSObject<UICollectionViewDataSource, UICollectionViewDelegate>
-(instancetype)initWithForecastItems:(NSArray *)forecastItems;
@end
