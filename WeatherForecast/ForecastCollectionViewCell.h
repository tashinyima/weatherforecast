//
//  WeatherCollectionViewCell.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ForecastItem;
@interface ForecastCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIView * vwBack;
@property (nonatomic, weak) IBOutlet UIImageView * imgIcon;
@property (nonatomic, weak) IBOutlet UILabel * lblTime;
@property (nonatomic, weak) IBOutlet UILabel * lblWeatherMain;
@property (nonatomic, weak) IBOutlet UILabel * lblWeatherDesc;
@property (nonatomic, weak) IBOutlet UILabel * lblWeatherHumidity;
@property (nonatomic, weak) IBOutlet UILabel * lblWeatherTemp;
@property (nonatomic, weak) IBOutlet UILabel * lblWeatherTempMin;
@property (nonatomic, weak) IBOutlet UILabel * lblWeatherTempMax;
@property (nonatomic, weak) IBOutlet UILabel * lblWeatherSpeed;
@property (nonatomic, weak) IBOutlet UILabel * lblWeatherSpeedDeg;
@property (nonatomic, weak) IBOutlet UILabel * lblWeatherPressure;

@property (nonatomic, strong) ForecastItem * forecast;
@end
