//
//  WAGetCitiesService.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import "CitiesService.h"
#import <CHCSVParser/CHCSVParser.h>
#import "City.h"
#import "CoreDataManager.h"
@interface CitiesService()<CHCSVParserDelegate>
@property (nonatomic, strong) DownloadCitiesSuccessBlock successBlock;
@property (nonatomic, strong) DownloadCitiesFailureBlock failureBlock;
@property (nonatomic, strong) DownloadCitiesProcessRawDataBlock rawDataBlock;
@property (nonatomic, strong) NSMutableArray * cities;
@property (nonatomic, strong) NSString * url;
//@property (nonatomic, strong) WACity * city;
@property (nonatomic, strong) NSMutableDictionary * city;
@property (nonatomic, strong) CoreDataManager * coreDataManager;
@end

@implementation CitiesService

-(instancetype)initWithUrl:(NSString *)url success:(DownloadCitiesSuccessBlock)successHandler failure:(DownloadCitiesFailureBlock)failureHandler rawData:(DownloadCitiesProcessRawDataBlock)rawDataHandler{
    self = [super init];
    if(self){
        _url = url;
        _successBlock = successHandler;
        _failureBlock = failureHandler;
        _rawDataBlock = rawDataHandler;
        _coreDataManager = [[CoreDataManager alloc] init];
    }
    return self;
}

-(void)startProcess{
    NSURLSessionTask * getTask = [self createDataTask:[self createRequest:_url]];
    [getTask resume];
}
-(NSURLRequest *)createRequest:(NSString *)urlStr{
    NSURL * url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30.0];
    return request;
}
-(NSURLSessionDataTask *)createDataTask:(NSURLRequest *)request{
    
    __weak CitiesService * weakSelf = self;
    NSURLSessionDataTask * getTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error){
            if(weakSelf.failureBlock)
                weakSelf.failureBlock(error);
        }else{
            if(weakSelf.rawDataBlock){ // Allow pre-processing of data(e.g. unzipping) before parsing
                data = weakSelf.rawDataBlock(data);
            }
            [weakSelf parseTSVCities:[[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding]];
            
        }
    }];
    return getTask;
}

-(void)parseTSVCities:(NSString *)data{
    //Parse TSV content
    CHCSVParser * parser = [[CHCSVParser alloc] initWithDelimitedString:data delimiter:'\t'];
    parser.delegate = self;
    [parser parse];
}


- (void)parser:(CHCSVParser *)parser didBeginLine:(NSUInteger)recordNumber{
    //Skip first (column names) row
    if(recordNumber > 1)
        _city = [NSMutableDictionary dictionary];

}

- (void)parser:(CHCSVParser *)parser didEndLine:(NSUInteger)recordNumber{
    //Release city object
    if(_city)
    [_cities addObject:_city];
        _city = nil;
}

- (void)parser:(CHCSVParser *)parser didReadField:(NSString *)field atIndex:(NSInteger)fieldIndex{
    switch (fieldIndex) {
        case 0:{ //id
            [_city setValue:field forKey:@"id"];
        }break;
        case 1://nm
            [_city setValue:field forKey:@"nm"];
            break;
        case 2://lat
            [_city setValue:@([field doubleValue]) forKey:@"lat"];
            break;
        case 3://lon
            [_city setValue:@([field doubleValue]) forKey:@"lon"];
            break;
        case 4://countrycode
            [_city setValue:field forKey:@"countryCode"];
            break;
            
        default:
            break;
    }
}
- (void)parser:(CHCSVParser *)parser didFailWithError:(NSError *)error{
    _failureBlock(error);
}

- (void)parserDidBeginDocument:(CHCSVParser *)parser{
    _cities = [NSMutableArray array];

}
- (void)parserDidEndDocument:(CHCSVParser *)parser{
    //All rows have been parsed and entities created. Save to context
    [_coreDataManager saveAllEntities:@"City" withPayloads:_cities];
    [_coreDataManager saveContext];
    if(_successBlock)
        _successBlock([_coreDataManager entities:@"City"]);
}
@end
