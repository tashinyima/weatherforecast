//
//  WAWeatherViewController.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"
#import "Forecast.h"
#import "ForecastService.h"

@interface ForecastViewController : UIViewController
@property (nonatomic, strong) City * city;
@property (nonatomic, strong) Forecast * forecast;
@end
