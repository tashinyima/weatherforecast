//
//  WAWeatherViewController.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import "ForecastViewController.h"
#import "DaysDataSource.h"
#import "ForecastDataSource.h"
#import "Constants.h"
#import "NSString+URLEncode.h"
#import "NSDate+Utils.h"

@interface ForecastViewController()<DayDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView * cvDays;
@property (weak, nonatomic) IBOutlet UICollectionView * cvWeather;
@property (strong, nonatomic) DaysDataSource * dsDays;
@property (strong, nonatomic) ForecastDataSource * dsWeather;
@property (nonatomic, strong) ForecastService * forecastService;
@property (nonatomic, strong) NSMutableDictionary * groupedWeather;
@property (nonatomic, strong) NSMutableArray * arrDays;

@end

@implementation ForecastViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"%@, %@",_city.name, _city.countrycode];
    [self loadWeatherDetails];
    [_cvDays registerNib:[UINib nibWithNibName:@"DayCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"Day"];
    [_cvWeather registerNib:[UINib nibWithNibName:@"ForecastCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"Forecast"];
    
}

- (void)loadWeatherDetails{
    //Weak self reference to call methods in blocks
    __weak ForecastViewController * weakSelf = self;
    //Call forecast service for supplied city id
    _forecastService = [[ForecastService alloc] initWithUrl:[NSString stringWithFormat:FORECAST_WEATHER_BY_ID,[_city.wacityID urlencode],OWA_APP_KEY] success:^(Forecast *forecast) {
        _forecast = forecast;
        [weakSelf groupWeatherDays];
        
        
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(),^{
            //Display error recieved
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Error"
                                                  message:error.localizedDescription
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
            [weakSelf presentViewController:alertController animated:YES completion:^{
                
            }];
        });
    }];
    //Start service process
    [_forecastService startProcess];
}

- (void)groupWeatherDays{
    _arrDays = [[NSMutableArray alloc] init];
    _groupedWeather = [[NSMutableDictionary alloc] init];
    for(ForecastItem * item in _forecast.list){
        NSMutableArray * arrItems = _groupedWeather[item.dt.dateString];
        if(arrItems == nil){
            arrItems = [NSMutableArray array];
        }
        [arrItems addObject:item];
        _groupedWeather[item.dt.dateString] = arrItems;
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    for(NSString * dateString in _groupedWeather.allKeys){
        [_arrDays addObject:[formatter dateFromString:dateString]];
    }
    [_arrDays sortUsingComparator:^NSComparisonResult(NSDate  * _Nonnull obj1, NSDate *  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    _dsDays = [[DaysDataSource alloc] initWithDays:_arrDays delegate:self];
    [_cvDays setDataSource:_dsDays];
    [_cvDays setDelegate:_dsDays];
    dispatch_async(dispatch_get_main_queue(), ^{
        [_cvDays reloadData];
    });
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DayDelegate
-(void)dayTapped:(NSDate *)date{
    NSArray * forecastItems = _groupedWeather[date.dateString];
    _dsWeather = [[ForecastDataSource alloc] initWithForecastItems:forecastItems];
    [_cvWeather setDataSource:_dsWeather];
    [_cvWeather setDelegate:_dsWeather];
    dispatch_async(dispatch_get_main_queue(), ^{
        [_cvWeather reloadData];
    });
}

@end
