//
//  Forecast.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import "Forecast.h"

@implementation Forecast

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self) {
        _code = [dictionary valueForKey:@"cod"];
        _message = [[dictionary valueForKey:@"message"] doubleValue];
        _count =[[dictionary valueForKey:@"cnt"] intValue];
        _city = [[ForecastCity alloc] initWithDictionary:[dictionary valueForKey:@"city"]] ;
        NSMutableArray * arr  = [NSMutableArray array];
        for(NSDictionary * dict in [dictionary valueForKey:@"list"]){
            [arr addObject:[[ForecastItem alloc] initWithDictionary:dict]];
        }
        _list = arr;
    }
    return self;
}

@end


@implementation ForecastCoord
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
        _latitude = [[dictionary valueForKey:@"lat"] doubleValue];
        _longitude = [[dictionary valueForKey:@"lon"] doubleValue];
    }
    return self;
    
}

@end

@implementation ForecastCity
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
        _identifier = [[dictionary valueForKey:@"id"] intValue];
        _name = [dictionary valueForKey:@"name"];
        _coord = [[ForecastCoord alloc] initWithDictionary:[dictionary valueForKey:@"coord"]];
        _country = [dictionary valueForKey:@"country"];
    }
    return self;
    
}
@end

@implementation ForecastItemMain

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
        _temp = [[dictionary valueForKey:@"temp"] doubleValue];
        _temp_min = [[dictionary valueForKey:@"temp_min"] doubleValue];
        _temp_max = [[dictionary valueForKey:@"temp_max"] doubleValue];
        _pressure = [[dictionary valueForKey:@"pressure"] doubleValue];
        _sea_level =[[dictionary valueForKey:@"sea_level"] doubleValue];
        _grnd_level = [[dictionary valueForKey:@"grnd_level"] doubleValue];
        _humidity = [[dictionary valueForKey:@"humidity"] intValue];
        _temp_kf = [[dictionary valueForKey:@"temp_kf"] doubleValue];
    }
    return self;
    
}
@end

@implementation ForecastItemWeather
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
        _identifier = [[dictionary valueForKey:@"id"] intValue];
        _main = [dictionary valueForKey:@"main"];
        _desc = [dictionary valueForKey:@"description"];
        _icon = [dictionary valueForKey:@"icon"];
    }
    return self;
    
}
@end
@implementation ForecastItemWind
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
        _speed = [[dictionary valueForKey:@"speed"] doubleValue];
        _deg = [[dictionary valueForKey:@"deg"] doubleValue];
    }
    return self;
    
}
@end
@implementation ForecastItemRain
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
        _threehr = [[dictionary valueForKey:@"3h"] doubleValue];
    }
    return self;
    
}
@end
@implementation ForecastItemCloud
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
        _all = [[dictionary valueForKey:@"all"] intValue];
    }
    return self;
    
}
@end
@implementation ForecastItemSnow
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
        _threehr = [[dictionary valueForKey:@"3h"] doubleValue];
    }
    return self;
    
}
@end
@implementation ForecastItem
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
        NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        _dt = [NSDate dateWithTimeIntervalSince1970:[[dictionary valueForKey:@"dt"] doubleValue]];
        _dt_txt = [formatter dateFromString:[dictionary valueForKey:@"dt_txt"]];
        _main = ([dictionary valueForKey:@"main"])?[[ForecastItemMain alloc] initWithDictionary:[dictionary valueForKey:@"main"]]:nil;
        _clouds = ([dictionary valueForKey:@"clouds"])?[[ForecastItemCloud alloc] initWithDictionary:[dictionary valueForKey:@"clouds"]]:nil;
        _wind = ([dictionary valueForKey:@"wind"])?[[ForecastItemWind alloc] initWithDictionary:[dictionary valueForKey:@"wind"]]:nil;
        _rain = ([dictionary valueForKey:@"rain"])?[[ForecastItemRain alloc] initWithDictionary:[dictionary valueForKey:@"rain"]]:nil;
        _snow = ([dictionary valueForKey:@"snow"])?[[ForecastItemSnow alloc] initWithDictionary:[dictionary valueForKey:@"snow"]]:nil;
        NSMutableArray * arr = [NSMutableArray array];
        for(NSDictionary * dict in [dictionary valueForKey:@"weather"]){
                [arr addObject:[[ForecastItemWeather alloc] initWithDictionary:dict]];
        }
        _weather = arr;

    }
    return self;
    
}
@end
