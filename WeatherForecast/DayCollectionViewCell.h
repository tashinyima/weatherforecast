//
//  DayCollectionViewCell.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DayCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel * lblDay;
@property (strong, nonatomic) NSDate * date;
@end
