//
//  WACoreDataManager.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import "CoreDataManager.h"
#define IS_OBJECT(T) _Generic( (T), id: YES, default: NO)
@implementation CoreDataManager
#pragma mark - Core Data stack
@synthesize mainManagedObjectContext = _mainManagedObjectContext;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

-(instancetype)init{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(contextDidSave:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:nil];
    }
    return self;
}
- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.Tashi.Niyma.TypiCode" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"WADataModel.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

- (NSManagedObjectContext *)mainManagedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_mainManagedObjectContext != nil) {
        return _mainManagedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _mainManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainManagedObjectContext setPersistentStoreCoordinator:coordinator];
    return _mainManagedObjectContext;
}

-(Base *)saveEntity:(NSString *)name withPayload:(NSDictionary *)dictionary{
    // Get class from name
    Class class = NSClassFromString(name);
    //Get entity identification attribute in core data
    NSString * entityId = [class performSelector:@selector(entityIdentificationAttribute)];
    //Get entity identification attribute in given dictionary
    NSString * responseId = [class performSelector:@selector(responseIdentificationAttribute)];
    //Mapping information to map dictionary to managed object
    NSDictionary * mapping = [class performSelector:@selector(mapping)];
    //Request to check for an exisiting managed object
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:name];
    request.predicate = [NSPredicate predicateWithFormat:@"%@ == %@",entityId, dictionary[responseId]];
    request.fetchLimit = 1;
    NSError * error;
    NSArray * objects = [[self managedObjectContext] executeFetchRequest:request error:&error];
    Base * obj;
    if(objects.count > 0){
        obj = objects.firstObject;
        
    }else{
        //Object did not exist in core data, therefore creating new
        obj = [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:[self managedObjectContext]];
        
    }
    //Map dictionary to managed object
    [self mapObject:obj withDictionary:dictionary mapping:mapping];
    return obj;
}
-(Base *)createEntity:(NSString *)name withPayload:(NSDictionary *)dictionary{
    // Get class from name
    Class class = NSClassFromString(name);
    //Create new entity with name
    __strong Base * obj = [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:[self managedObjectContext]];
    //If entity has mapping information perform mapping
    if([class respondsToSelector:@selector(mapping)]){
        NSDictionary * mapping = [class performSelector:@selector(mapping)];
        [self mapObject:obj withDictionary:dictionary mapping:mapping];
    }
    return obj;
}
-(void)mapObject:(inout Base *)obj withDictionary:(NSDictionary *)dictionary mapping:(NSDictionary *)mapping{
    NSEntityDescription * description  = [NSEntityDescription entityForName:NSStringFromClass([obj class]) inManagedObjectContext:[self managedObjectContext]];
    //For all mapping keys map dictionary value to object property
    for(NSString * key in mapping.allKeys){
        if([mapping[key] isKindOfClass:[NSString class]]){
            id value = [dictionary valueForKeyPath:mapping[key]];
            if(value){
                if([obj valueForKey:key]){
                    [obj setValue:nil forKey:key];
                }
                [obj setValue:value forKey:key];
            }
        }else if([mapping[key] isKindOfClass:[NSNumber class]]){
            id value = [dictionary valueForKeyPath:mapping[key]];
            if(value){
                if([obj valueForKey:key]){
                    [obj setValue:nil forKey:key];
                }
                [obj setValue:value forKey:key];
            }
            
        }else{
            //If mapping information is a dictionary, then we are mapping a subentity
            Base * subobj = [obj valueForKey:key];
            if(!subobj){
                NSDictionary * relations = [description  relationshipsByName];
                for(NSString * relName in relations.allKeys ) {
                    if([relName isEqualToString:key]){
                        NSRelationshipDescription * relDesc = relations[relName];
                        subobj = [NSEntityDescription insertNewObjectForEntityForName:relDesc.destinationEntity.name inManagedObjectContext:[self managedObjectContext]];
                        break;
                    }
                }
            }
            //Map subentity
            [self mapObject:subobj withDictionary:dictionary[key] mapping:mapping[key]];
            [obj setValue:subobj forKey:key];
        }
    }

}

-(NSArray *)saveAllEntities:(NSString *)name withPayloads:(NSArray *)dictionaries{
    NSError * error;
    //Fetch all entities of given name
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:name];
    NSArray * objects = [[self managedObjectContext] executeFetchRequest:request error:&error];
    //Delete all returned objects
    for(NSManagedObject * object in objects){
        [[self managedObjectContext] deleteObject:object];
    }
    [self saveContext];
    //For all dictionaries save/create entity
    NSMutableArray * retObjects = [NSMutableArray array];
    for(NSDictionary * dictionary in dictionaries){
        [retObjects addObject:[self createEntity:name withPayload:dictionary]];
    }
    [self saveContext];
    
    return retObjects;
}
-(NSManagedObject *)entity:(NSString *)name attribute:(NSString *)attribute value:(id)value{
    NSError * error;
    //Fetch first entity with given attribute and value
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:name];
    request.predicate = [NSPredicate predicateWithFormat:@"%K == %@",attribute, value];
    request.fetchLimit = 1;
    NSArray * objects = [[self managedObjectContext] executeFetchRequest:request error:&error];
    return objects.firstObject;
    
}
-(NSArray *)entities:(NSString *)name page:(int)pageno size:(int)pagesize{
    NSError * error;
    //Fetch request with entity name
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:name];
    //Don't return faults
    request.returnsObjectsAsFaults = NO;
    //Include property values
    request.includesPropertyValues = YES;
    //Include subentities
    request.includesSubentities = YES;
    //Fetch given page size
    request.fetchLimit = pagesize;
    //Offset to given page no
    request.fetchOffset = pageno * pagesize;
    NSArray * objects = [[self mainManagedObjectContext] executeFetchRequest:request error:&error];
    
    return objects;
}
-(NSArray *)entities:(NSString *)name{
    NSError * error;
    //Fetch request with entity name to get all entities
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:name];
    request.returnsObjectsAsFaults = NO;
    request.includesPropertyValues = YES;
    request.includesSubentities = YES;
    NSArray * objects = [[self mainManagedObjectContext] executeFetchRequest:request error:&error];
    
    return objects;
}
-(NSArray *)entities:(NSString *)name withPredicate:(NSPredicate * )predicate{
    NSError * error;
    //Fetch request with entity name to get all entities
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:name];
    request.returnsObjectsAsFaults = NO;
    request.includesPropertyValues = YES;
    request.includesSubentities = YES;
    request.predicate = predicate;
    NSArray * objects = [[self mainManagedObjectContext] executeFetchRequest:request error:&error];
    
    return objects;
}
#pragma mark - Core Data Saving support
- (void)contextDidSave:(NSNotification *)notification
{
    //Merge contexts
    SEL selector = @selector(mergeChangesFromContextDidSaveNotification:);
    [self.managedObjectContext performSelectorOnMainThread:selector withObject:notification waitUntilDone:YES];
    [self.mainManagedObjectContext performSelectorOnMainThread:selector withObject:notification waitUntilDone:YES];
    
}
- (void)saveContext{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
    [managedObjectContext performBlockAndWait:^{
            NSError *error = nil;
            if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
    }];
    }
    managedObjectContext = self.mainManagedObjectContext;
    if (managedObjectContext != nil) {
        [managedObjectContext performBlockAndWait:^{
            NSError *error = nil;
            if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
        }];
    }
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
