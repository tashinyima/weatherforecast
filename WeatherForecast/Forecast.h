//
//  Forecast.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ForecastCity;
//Corresponding to top-level dictionary in forecast response
@interface Forecast : NSObject
@property (nonatomic, strong) NSString * code;
@property (nonatomic, strong) ForecastCity * city;
@property (nonatomic, assign) double message;
@property (nonatomic, assign) int count;
@property (nonatomic, strong) NSArray * list;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end

//Corresponding to coord dictionary in forecast -> city response
@interface ForecastCoord : NSObject
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end

//Corresponding to city dictionary in forecast response
@interface ForecastCity : NSObject
@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) ForecastCoord * coord;
@property (nonatomic, strong) NSString * country;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end

//Corresponding to main dictionary in forecast->list->item response
@interface ForecastItemMain : NSObject
@property (nonatomic, assign) double temp;
@property (nonatomic, assign) double temp_min;
@property (nonatomic, assign) double temp_max;
@property (nonatomic, assign) double pressure;
@property (nonatomic, assign) double sea_level;
@property (nonatomic, assign) double grnd_level;
@property (nonatomic, assign) int humidity;
@property (nonatomic, assign) double temp_kf;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
//Corresponding to weather dictionary in forecast->list->item response
@interface ForecastItemWeather : NSObject
@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString * main;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, strong) NSString * icon;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end

//Corresponding to wind dictionary in forecast->list->item response
@interface ForecastItemWind : NSObject
@property (nonatomic, assign) double speed;
@property (nonatomic, assign) double deg;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end

//Corresponding to rain dictionary in forecast->list->item response
@interface ForecastItemRain : NSObject
@property (nonatomic, assign) double threehr;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end

//Corresponding to clouds dictionary in forecast->list->item response
@interface ForecastItemCloud : NSObject
@property (nonatomic, assign) int all;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end

//Corresponding to snow dictionary in forecast->list->item response
@interface ForecastItemSnow : NSObject
@property (nonatomic, assign) double threehr;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end

//Corresponding to item dictionary in forecast->list collection response
@interface ForecastItem : NSObject
@property (nonatomic, strong) NSDate * dt;
@property (nonatomic, strong) ForecastItemMain * main;
@property (nonatomic, strong) NSArray<ForecastItemWeather *> * weather;
@property (nonatomic, strong) ForecastItemCloud * clouds;
@property (nonatomic, strong) ForecastItemWind * wind;
@property (nonatomic, strong) ForecastItemRain * rain;
@property (nonatomic, strong) ForecastItemSnow * snow;
@property (nonatomic, strong) NSDate * dt_txt;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end



