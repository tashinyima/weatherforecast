//
//  DaysDataSource.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol DayDelegate<NSObject>
-(void)dayTapped:(NSDate *)date;
@end
@interface DaysDataSource : NSObject<UICollectionViewDataSource, UICollectionViewDelegate>

-(instancetype) initWithDays:(NSArray *)days delegate:(id<DayDelegate>)delegate;
@end
