//
//  WAGetCitiesService.h
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^DownloadCitiesSuccessBlock)(NSArray * cities);
typedef id (^DownloadCitiesProcessRawDataBlock)(NSData * data);
typedef void (^DownloadCitiesFailureBlock)(NSError * error);



@interface CitiesService : NSObject

@property (nonatomic, readonly) DownloadCitiesSuccessBlock successBlock;
@property (nonatomic, readonly) DownloadCitiesFailureBlock failureBlock;
@property (nonatomic, readonly) DownloadCitiesProcessRawDataBlock rawDataBlock;


-(instancetype)initWithUrl:(NSString *)url success:(DownloadCitiesSuccessBlock)successHandler failure:(DownloadCitiesFailureBlock)failureHandler rawData:(DownloadCitiesProcessRawDataBlock)rawDataHandler;

//Creates GET request operation based on url request
-(NSURLSessionDataTask *)createDataTask:(NSURLRequest *)request;

//Creates request based on url
-(NSURLRequest *)createRequest:(NSString *) url;

//Begins fetch and parsing process
-(void)startProcess;

-(void)parseTSVCities:(NSString *)data;
@end
