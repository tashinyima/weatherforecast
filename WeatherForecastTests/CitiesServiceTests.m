//
//  CitiesServiceTests.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <OHHTTPStubs/OHHTTPStubs.h>
#import "OHPathHelpers.h"
#import "Constants.h"
#import "CitiesService.h"
#import "City.h"
@interface CitiesServiceTests : XCTestCase

@end

@implementation CitiesServiceTests

- (void)setUp {
    [super setUp];
    [OHHTTPStubs removeAllStubs];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    [OHHTTPStubs removeAllStubs];
}

- (void)testCitiesService {
    XCTestExpectation *expectation
    = [self expectationWithDescription:@"Create user API request"];

    DownloadCitiesSuccessBlock successBlock = ^(NSArray * cities){
        XCTAssertNotNil(cities, @"Cities are null");
        XCTAssertTrue([cities isKindOfClass:[NSArray class]],@"Cities are not a colleciton");
        XCTAssert([[cities firstObject] isKindOfClass:[City class]],@"City object not created");
        [expectation fulfill];
    };
    DownloadCitiesFailureBlock errorBlock = ^(NSError * error){
        XCTAssertNil(error, @"Error was not null");
        [expectation fulfill];
    };
    DownloadCitiesProcessRawDataBlock rawDataBlock = ^id (NSData * data){
        return data;
    };
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [request.URL.absoluteString isEqualToString:CITIES_LIST];
    } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSString* fixture = OHPathForFile(@"City", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture
                                                statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    CitiesService * service = [[CitiesService alloc] initWithUrl:CITIES_LIST success:successBlock failure:errorBlock rawData:rawDataBlock];
    
    
    [service startProcess];
    [self waitForExpectationsWithTimeout:10
                                 handler:^(NSError *error) {
                                     if (error) {
                                         XCTFail(@"timeout error: %@", error);
                                     }
                                 }];

}




@end
