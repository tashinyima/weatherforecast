//
//  ForecastTests.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Forecast.h"

@interface ForecastTests : XCTestCase
@property (nonatomic, strong) Forecast * forecast;
@end

@implementation ForecastTests

- (void)setUp {
    [super setUp];
    NSData * data = [NSData dataWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForResource:@"forecast" ofType:@"json"]];
    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    _forecast = [[Forecast alloc] initWithDictionary:dict];

    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testForecast {
    XCTAssertNotNil(_forecast, @"Forecast not parsed");
    XCTAssertTrue([_forecast.code isEqualToString:@"200"],@"Code not parsed correctly");
    XCTAssertTrue((_forecast.message == 0.006),@"Message not parsed correctly");
    XCTAssertTrue((_forecast.count == 40),@"Count not parsed correctly");
    XCTAssertTrue(([_forecast.list isKindOfClass:[NSArray class]]),@"List not parsed correctly");

}
- (void)testForecastItem{
    ForecastItem * item = _forecast.list.firstObject;

    XCTAssertNotNil(item,@"Item parsed was nil");
    XCTAssertNotNil(item.dt, @"Dt was nil");
    XCTAssertTrue([item.dt timeIntervalSince1970] == 1457038800,@"Dt not parsed correctly" );
    XCTAssertNotNil(item.weather,@"Weather was nil" );
    XCTAssertTrue([item.weather isKindOfClass:[NSArray class]],@"Weather is not of collection type" );
    XCTAssertTrue([item.dt_txt timeIntervalSince1970] == 1457020800,@"Dt_txt not parsed correctly" );
    XCTAssertNotNil(item.main,@"Main was nil" );
    XCTAssertNotNil(item.clouds,@"Clouds was nil" );
    XCTAssertNotNil(item.wind,@"Wind was nil" );
    XCTAssertNil(item.rain,@"Rain was not nil" );
    XCTAssertNil(item.snow,@"Snow was not nil" );

}

- (void)testForecastCoords{
    ForecastCity * city = _forecast.city;
    ForecastCoord * coords = city.coord;

    XCTAssertNotNil(coords, @"Coords were nil");
    XCTAssertTrue((coords.latitude == 40.88232),@"Latitude not parsed correctly");
    XCTAssertTrue((coords.longitude == -74.083198999999993),@"Longitude not parsed correctly");
}

- (void)testForecastCity{
    ForecastCity * city = _forecast.city;
    XCTAssertNotNil(city, @"City was nil");
    XCTAssertTrue(city.identifier == 5100604,@"Identifier not parsed correctly" );
    XCTAssertTrue([city.name isEqualToString:@"Lodi"],@"Name not parsed correctly" );
   XCTAssertTrue([city.country isEqualToString:@"US"],@"Country not parsed correctly" );
}

-(void)testForecastItemMain{
    ForecastItem * item = _forecast.list.firstObject;
    ForecastItemMain * main = item.main;
    XCTAssertNotNil(main,@"Main was nil");
    XCTAssertTrue(main.temp == 275.02,@"Temp not parsed correctly");
    XCTAssertTrue(main.temp_min == 275.02,@"Temp_Min not parsed correctly");
    XCTAssertTrue(main.temp_max == 275.02,@"Temp_Max not parsed correctly");
    XCTAssertTrue(main.pressure == 1022.12,@"Pressure not parsed correctly");
    XCTAssertTrue(main.sea_level == 1036.67,@"Sea Level not parsed correctly");
    XCTAssertTrue(main.grnd_level == 1022.12,@"Ground Level not parsed correctly");
    XCTAssertTrue(main.humidity == 52,@"Humidity not parsed correctly");
    XCTAssertTrue(main.temp_kf == 0,@"Temp_Kf not parsed correctly");
}
-(void)testForecastItemWeather{
    ForecastItem * item = _forecast.list.firstObject;
    XCTAssertNotNil(item.weather, @"Weather collection was nil");
    XCTAssertTrue([item.weather isKindOfClass:[NSArray class]], @"Weather was not a collection");
    XCTAssertTrue(item.weather.count == 1, @"Incorrect Weather count");
    ForecastItemWeather * weather = item.weather.firstObject;
    XCTAssertNotNil(weather, @"Weather was nil");
    XCTAssertTrue(weather.identifier == 802, @"Identifier not parsed correctly");
    XCTAssertTrue([weather.main isEqualToString:@"Clouds"], @"Main not parsed correctly");
    XCTAssertTrue([weather.desc isEqualToString:@"scattered clouds"], @"Description not parsed correctly");
    XCTAssertTrue([weather.icon isEqualToString:@"03d"], @"Icon not parsed correctly");

}
-(void)testForecastItemWind{
    ForecastItem * item = _forecast.list.firstObject;
    XCTAssertNotNil(item.wind, @"Wind was nil");
    ForecastItemWind * wind = item.wind;
    XCTAssertTrue(wind.speed == 1.52, @"Speed not parsed correctly");
    XCTAssertTrue(wind.deg == 240.501, @"Deg not parsed correctly");
}
-(void)testForecastItemRain{
    for(ForecastItem * item in _forecast.list){
        if(item.rain){
            ForecastItemRain * rain = item.rain;
            XCTAssertTrue(rain.threehr != 0,@"3h not parsed correctly");
            break;
        }
    }
}
-(void)testForecastItemCloud{
    ForecastItem * item = _forecast.list.firstObject;
    XCTAssertNotNil(item.clouds, @"Clouds was nil");
    ForecastItemCloud * cloud = item.clouds;
    XCTAssertTrue(cloud.all == 48, @"All not parsed correctly");
    
}
-(void)testForecastItemSnow{
    for(ForecastItem * item in _forecast.list){
        if(item.snow){
            ForecastItemSnow * snow = item.snow;
            XCTAssertTrue(snow.threehr != 0,@"3h not parsed correctly");
            break;
        }
    }
  
    
}


- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
