//
//  CoreDataManagerTests.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "City.h"
#import "MemCoreDataManager.h"

@interface CoreDataManagerTests : XCTestCase
@property (nonatomic, strong) MemCoreDataManager * coreDataManager;
@end

@implementation CoreDataManagerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _coreDataManager = [[MemCoreDataManager alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testEntityCreate {
    City * city = (City *)[_coreDataManager saveEntity:@"City" withPayload:@{@"id":@"1",@"nm":@"new",@"lat":@35,@"lon":@25,@"countryCode":@"UK"}];
    XCTAssertNotNil(city, @"City object not created");
    XCTAssertTrue([city.wacityID isEqualToString:@"1"],@"ID not parsed correctly");
    XCTAssertTrue([city.name isEqualToString:@"new"],@"Name not parsed correctly");
    XCTAssertTrue(([city.latitude compare:@35] == NSOrderedSame),@"Latitude not parsed correctly");
    XCTAssertTrue(([city.longitude compare:@25] == NSOrderedSame),@"Longitude not parsed correctly");
    XCTAssertTrue([city.countrycode isEqualToString:@"UK"],@"Latitude not parsed correctly");
}
- (void)testEntitySave {
    
}
- (void)testEntitiesSave{
    NSArray * entitiesDicts = @[@{@"id":@"1",@"nm":@"new1",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"2",@"nm":@"new2",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"3",@"nm":@"new3",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"4",@"nm":@"new4",@"lat":@35,@"lon":@25,@"countryCode":@"UK"}
                                ];
    NSArray * cities = [_coreDataManager saveAllEntities:@"City" withPayloads:entitiesDicts];
    XCTAssertNotNil(cities, @"City objects not created");
    XCTAssertTrue((cities.count == 4), @"All dictionaries not created");
}

- (void)testGetEntities{
    NSArray * entitiesDicts = @[@{@"id":@"1",@"nm":@"new1",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"2",@"nm":@"new2",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"3",@"nm":@"new3",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"4",@"nm":@"new4",@"lat":@35,@"lon":@25,@"countryCode":@"UK"}
                                ];
    NSArray * cities = [_coreDataManager saveAllEntities:@"City" withPayloads:entitiesDicts];

    cities = [_coreDataManager entities:@"City"];
    XCTAssertNotNil(cities, @"City objects not fetched");
    XCTAssertTrue((cities.count == 4), @"All dictionaries not created");

}
- (void)testGetPagedEntities{
    NSArray * entitiesDicts = @[@{@"id":@"1",@"nm":@"new1",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"2",@"nm":@"new2",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"3",@"nm":@"new3",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"4",@"nm":@"new4",@"lat":@35,@"lon":@25,@"countryCode":@"UK"}
                                ];
    NSArray * cities = [_coreDataManager saveAllEntities:@"City" withPayloads:entitiesDicts];

    cities = [_coreDataManager entities:@"City" page:1 size:1];
    XCTAssertNotNil(cities, @"City objects not fetched");
    XCTAssertTrue((cities.count == 1), @"Results not paged");

}
- (void)testGetEntitiesWithPredicate{
    NSArray * entitiesDicts = @[@{@"id":@"1",@"nm":@"new1",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"2",@"nm":@"new2",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"3",@"nm":@"new3",@"lat":@35,@"lon":@25,@"countryCode":@"UK"},
                                @{@"id":@"4",@"nm":@"new4",@"lat":@35,@"lon":@25,@"countryCode":@"UK"}
                                ];
    NSArray * cities = [_coreDataManager saveAllEntities:@"City" withPayloads:entitiesDicts];
    
    cities = [_coreDataManager entities:@"City" withPredicate:[NSPredicate predicateWithFormat:@"name LIKE 'new2'"]];
    XCTAssertNotNil(cities, @"City objects not fetched");
    XCTAssertTrue((cities.count == 1), @"Results count incorrect");
    City * city = cities.firstObject;
    XCTAssertNotNil(city, @"City object not fetched");
    XCTAssertTrue([city.wacityID isEqualToString:@"2"],@"ID not parsed correctly");
    XCTAssertTrue([city.name isEqualToString:@"new2"],@"Name not parsed correctly");
    XCTAssertTrue(([city.latitude compare:@35] == NSOrderedSame),@"Latitude not parsed correctly");
    XCTAssertTrue(([city.longitude compare:@25] == NSOrderedSame),@"Longitude not parsed correctly");
    XCTAssertTrue([city.countrycode isEqualToString:@"UK"],@"Latitude not parsed correctly");

}
- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
