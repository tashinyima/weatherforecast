//
//  ForecastServiceTests.m
//  WeatherForecast
//
//  Created by Tashi.Niyma on 3/17/17.
//  Copyright © 2017 Tashi.Niyma. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <OHHTTPStubs/OHHTTPStubs.h>
#import "OHPathHelpers.h"
#import "Constants.h"
#import "ForecastService.h"
@interface ForecastServiceTests : XCTestCase

@end

@implementation ForecastServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    [OHHTTPStubs removeAllStubs];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    [OHHTTPStubs removeAllStubs];
}

- (void)testWeatherService {
    
    XCTestExpectation *expectation
    = [self expectationWithDescription:@"Create user API request"];
    
    GetForecastSuccessBlock successBlock = ^(Forecast * forecast){
        [expectation fulfill];
        XCTAssertNotNil(forecast, @"Forecast is null");
        XCTAssertTrue([forecast isKindOfClass:[Forecast class]],@"Incorrect forecast type");
    };
    GetForecastFailureBlock errorBlock = ^(NSError * error){
        [expectation fulfill];
        XCTAssertNil(error, @"Error was not null");
        
    };
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [request.URL.absoluteString isEqualToString:[NSString stringWithFormat:FORECAST_WEATHER_BY_ID,@"5100604",OWA_APP_KEY]];
    } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSString* fixture = OHPathForFile(@"forecast.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture
                                                statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    ForecastService * service = [[ForecastService alloc] initWithUrl:[NSString stringWithFormat:FORECAST_WEATHER_BY_ID,@"5100604",OWA_APP_KEY] success:successBlock failure:errorBlock];
    [service startProcess];
    [self waitForExpectationsWithTimeout:5
                                 handler:^(NSError *error) {
                                     if (error) {
                                         XCTFail(@"timeout error: %@", error);
                                     }
                                 }];
    
}
@end
